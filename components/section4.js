import Author from "./_child/author"
import Link from "next/link"
import Image from "next/image"

export default function section4() {
  return (
    <section className="container mx-auto md:px-20 py-16">
        <div className="grid lg:grid-cols-2">
            <div className="item">
                <h1 className="font-bold text-4xl py-12">Food</h1>
                <div className="flex flex-col gap-6">
                    {/* posts */}
                    { Post() }
                    { Post() }
                    { Post() }
                    { Post() }
                </div>
            </div>
            <div className="item">
                <h1 className="font-bold text-4xl py-12">Travel</h1>
                <div className="flex flex-col gap-6">
                    { Post() }
                    { Post() }
                    { Post() }
                    { Post() }
                </div>
            </div>
        </div>
    </section>
  )
}

function Post(){
    return (
        <div className="flex gap-5">
            <div className="image flex flex-col justify-start">
                <Link href={"../posts/page"}><Image src={"/images/img1.jpg"} className="rounded" width={300} height={250} /></Link>
            </div>
            <div className="info flex justify-center flex-col">
                <div className="cat">
                    <Link href={"../posts/page"} className="text-orange-600 hover:text-orange-800">Food, Travel</Link>
                    <Link href={"../posts/page"} className="text-gray-800 hover:text-gray-600"> - Desember 20, 2022</Link>
                </div>
                <div className="title">
                    <Link href={"../posts/page"} className="text-xl font-bold text-gray-800 hover:text-gray-600">Mangkuk Kehidupan</Link>
                </div>
                <Author></Author>
            </div>
        </div>
    )
}