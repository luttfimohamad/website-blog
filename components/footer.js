import { ImFacebook, ImTwitter, ImYoutube } from "react-icons/im";

export default function footer() {
    return (
      <footer className="bg-gray-200">
      <div className="container mx-auto flex justify-center py-12">
          <div className="py-5">
              <div className="flex gap-6 justify-center">
                <a href="/"><ImFacebook color="#000000" /></a>
                <a href="/"><ImTwitter color="#000000" /></a>
                <a href="/"><ImYoutube color="#000000" /></a>
              </div>

              <p className="py-5 text-gray-400">Copyright ©2023 All rights reserved | This template is made with  by Mohamad Lutfi</p>
              <p className="text-gray-400 text-center">Terms & Condition</p>
          </div>
      </div>

    </footer>
    )
  }