import Link from "next/link"
import Image from "next/image"
import Author from "./_child/author"
import getPost from "@/lib/helper"

export default function section2() {
    getPost(4).then(res => console.log(res))
  return (
    <section className="container mx-auto md:px-20 py-10">
        <h1 className="font-bold text-4xl py-12 text-center">Latest Posts</h1>

        {/* grid columns */}
        <div className="grid md:grid-cols-2 lg:grid-cols-3 gap-14">
            { Post() }
            { Post() }
            { Post() }
            { Post() }
            { Post() }
            { Post() }
            { Post() }
            { Post() }
        </div>
    </section>
  )
}


function Post(){
    return (
        <div className="item">
            <div className="images">
                <Link href={"../posts/page"}><Image src={"/images/img1.jpg"} className="rounded" width={500} height={350} /></Link>
            </div>
            <div className="info flex justify-center flex-col py-4">
                <div className="cat">
                    <Link href={"../posts/page"} className="text-orange-600 hover:text-orange-800">Food, Travel</Link>
                    <Link href={"../posts/page"} className="text-gray-800 hover:text-gray-600"> - Desember 20, 2022</Link>
                </div>
                <div className="title">
                    <Link href={"../posts/page"} className="text-xl font-bold text-gray-800 hover:text-gray-600">Mangkuk Kehidupan</Link>
                </div>
                <p className="text-gray-500 py-3">
                Di dalam mangkuk ini, harmoni tercipta antara kelezatan sayuran segar dan kegurihan daging yang lezat. 
                Seporsi kehidupan yang seimbang, berpadu dalam sentuhan cita rasa yang memikat lidah.
                </p>
                <Author />
            </div>
        </div>
    )
}