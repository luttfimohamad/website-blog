import Image from "next/image"
import Link from "next/link"

export default function author() {
  return (
    <div className="author flex py-5">
        <div >
            <img className="rounded-full" src={"/images/author.jpg"} width={60} height={60} />
        </div>       
        <div className="flex flex-col justify-center px-4">
            {/* <Link href={"/"}><a className="text-md font-bold text-gray-800 hover:text-gray-600">Flying High</a></Link> */}
            <a href="/" className="text-md font-bold text-gray-800 hover:text-gray-600">Mohamad Lutfi</a>
            <span className="text-sm text-gray-500">Food Blogger</span>
        </div>
    </div>
  )
}