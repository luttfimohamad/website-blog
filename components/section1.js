import Image from "next/image"
import Link from "next/link"
import Author from "@/components/_child/author"
import { Swiper, SwiperSlide } from 'swiper/react';
import { Pagination, Scrollbar, A11y} from 'swiper';
import 'swiper/css';
import 'swiper/css/pagination';

export default function section1() {
    const bg = {
        background: "url('/images/banner.jpg') no-repeat",
        backgroundSize: "cover",
        backgroundPosition: "right"
    }
  return (
    <section className="py-16" style={bg}>
        <div className="container mx-auto md:px-20">
            <h1 className="font-bold text-4xl pb-12 text-center">Trending</h1>

            <Swiper
                modules={[Pagination, Scrollbar, A11y]}
                spaceBetween={50}
                slidesPerView={1}
                pagination={{ clickable: true }}
                scrollbar={{ draggable: true }}
                onSwiper={(swiper) => console.log(swiper)}
                onSlideChange={() => console.log('slide change')}
                >
                <SwiperSlide className="Black-slide">{ Slide() }</SwiperSlide>
                <SwiperSlide>{ Slide() }</SwiperSlide>
                <SwiperSlide>{ Slide() }</SwiperSlide>
                <SwiperSlide>{ Slide() }</SwiperSlide>
                <SwiperSlide>{ Slide() }</SwiperSlide>
                <br/>
                <br/>
                <br/>
            </Swiper>


        </div>
    </section>
  )
}

function Slide(){
    return (
        <div className="grid md:grid-cols-2">
            <div className="image">
                {/* <Link href={"/pages/posts/page"}><a><Image src={"/images/img1.jpg"} width={600} height={600} /></a></Link> */}
                <a href="../posts/page"><img src="/images/img1.jpg" width={600} height={600}/></a>
            </div>
            <div className="info flex justify-center flex-col">
                <div className="cat">
                    {/* <Link href={"/pages/posts/page"}><a className="text-orange-600 hover:text-orange-800">Business, Travel</a></Link>
                    <Link href={"/pages/posts/page"}><a className="text-gray-800 hover:text-gray-600">- July 3, 2022</a></Link> */}
                    <a href="../posts/page" className="text-orange-600 hover:text-orange-800">Food, Travel</a>
                    <a href="../posts/page" className="text-gray-800 hover:text-gray-600"> - Desember 20, 2022</a>
                </div>
                <div className="title">
                    {/* <Link href={"/pages/posts/page"}><a className="text-3xl md:text-6xl font-bold text-gray-800 hover:text-gray-600">Your most unhappy customers are your greatest source of learning</a></Link> */}
                    <a href="../posts/page" className="text-3xl md:text-6xl font-bold text-gray-800 hover:text-gray-600">Mangkuk Kehidupan</a>
                </div>
                <p className="text-gray-500 py-3">
                Di dalam mangkuk ini, harmoni tercipta antara kelezatan sayuran segar dan kegurihan daging yang lezat. Seporsi kehidupan yang seimbang, berpadu dalam sentuhan cita rasa yang memikat lidah.
                <br/><br/>
                Lihatlah sayuran-sayuran berwarna-warni yang menjulang, menawarkan kehidupan dan vitalitas. Daun hijau merekah dan menyegarkan, menggoda selera dengan kelembutan dan krispi. Mereka memberikan kesegaran dan kebaikan alami, mengisi mangkuk ini dengan kesehatan dan keceriaan.
                <br/><br/>
                Tapi jangan lewatkan daging, bahan pokok yang mengundang kenikmatan dalam setiap gigitan. Daging yang empuk dan lezat, menggoda dengan aroma menggugah selera. Kelezatannya mengalir, menghadirkan kepuasan dan kekenyangan yang tiada tara.
                <br/><br/>
                Selamat menikmati setiap sendokan dari mangkuk ini, dengan cita rasa yang mempesona dan kesenangan yang tak terlupakan.
                </p>
                <Author />
            </div>
        </div>
    )
}