import { Swiper, SwiperSlide } from "swiper/react"
import Link from "next/link"
import Image from "next/image"
import Author from "./_child/author"

export default function section3() {
  return (
    <section className="container mx-auto md:px-20 py-16">
        <h1 className="font-bold text-4xl py-12 text-center">Most Popular</h1>

        {/* swiper */}
        <Swiper
            slidesPerView={2}
        >
            <SwiperSlide>{  Post() }</SwiperSlide>
            <SwiperSlide>{  Post() }</SwiperSlide>
            <SwiperSlide>{  Post() }</SwiperSlide>
            <SwiperSlide>{  Post() }</SwiperSlide>
            <SwiperSlide>{  Post() }</SwiperSlide>
        </Swiper>

    </section>
  )
}


function Post(){
    return (
        <div className="grid">
            <div className="images">
                <Link href={"../posts/page"}><Image src={"/images/img1.jpg"} width={600} height={400} /></Link>
            </div>
            <div className="info flex justify-center flex-col py-4">
                <div className="cat">
                    <Link href={"../posts/page"} className="text-orange-600 hover:text-orange-800">Food, Travel</Link>
                    <Link href={"../posts/page"} className="text-gray-800 hover:text-gray-600"> - Desember 20, 2022</Link>
                </div>
                <div className="title">
                    <Link href={"../posts/page"} className="text-3xl md:text-4xl font-bold text-gray-800 hover:text-gray-600">Mangkuk Kehidupan</Link>
                </div>
                <p className="text-gray-500 py-3">
                Di dalam mangkuk ini, harmoni tercipta antara kelezatan sayuran segar dan kegurihan daging yang lezat. Seporsi kehidupan yang seimbang, berpadu dalam sentuhan cita rasa yang memikat lidah.
                </p>
                <Author></Author>
            </div>
        </div>
    )
}