import { ImFacebook, ImTwitter, ImYoutube } from "react-icons/im";
import Link from 'next/link'

export default function header() {
  return (
    <header className="bg-gray-200">
        <div className="xl:container xl:mx-auto flex flex-col items-center sm:flex-row sm:justify-between text-center py-3">
            <div className="md:flex-none w-96 order-2 sm:order-1 flex justify-center py-4 sm:py-0">
                <input type="text" className="input-text"  placeholder="Search..."/>
            </div>
            <div className="shrink w-80 sm:order-2">
            {/* <Link href={"/"}>
                    <a className="font-bold uppercase text-3xl">Design</a>
                </Link> */}
                <a href="/" className="font-bold uppercase text-3xl">The Blogging Hub</a>
            </div>
            <div className="w-96 order-3 flex justify-center">
                <div className="flex gap-6">
                {/* <Link href={"/"}><a><ImFacebook color="#000000" /></a></Link>
                <Link href={"/"}><a><ImTwitter color="#000000" /></a></Link>                    
                <Link href={"/"}><a><ImYoutube color="#000000" /></a></Link> */}
                <a href="/"><ImFacebook color="#000000" /></a>
                <a href="/"><ImTwitter color="#000000" /></a>
                <a href="/"><ImYoutube color="#000000" /></a>
                </div>
            </div>
        </div>
    </header>
  )
}