
import Format from "@/layout/format"
import Author from "@/components/_child/author"
import Image from "next/image"
import Ralated from "@/components/_child/ralated"

export default function Page(){
    return (
        <Format>
            <section className='container mx-auto md:px-2 py-16 w-1/2'>
                <div className='flex justify-center'>
                    <Author></Author>
                </div>

                <div className="post py-10">
                    <h1 className='font-bold text-4xl text-center pb-5'>Mangkuk Kehidupan</h1>

                    <p className='text-gray-500 text-xl text-center'>Di dalam mangkuk ini, harmoni tercipta antara kelezatan sayuran segar dan kegurihan daging yang lezat, menyajikan perpaduan sempurna antara kesehatan dan kenikmatan dalam setiap sendokannya.</p>

                    <div className="py-10">
                        <Image src={"/images/img1.jpg"} width={900} height={600}></Image>
                    </div>

                    <div className="content text-gray-600 text-lg flex flex-col gap-4">
                        <p>
                        Di dalam mangkuk ini, harmoni tercipta antara kelezatan sayuran segar dan kegurihan daging yang lezat. Seporsi kehidupan yang seimbang, berpadu dalam sentuhan cita rasa yang memikat lidah.
                        <br/><br/>
                        Lihatlah sayuran-sayuran berwarna-warni yang menjulang, menawarkan kehidupan dan vitalitas. Daun hijau merekah dan menyegarkan, menggoda selera dengan kelembutan dan krispi. Mereka memberikan kesegaran dan kebaikan alami, mengisi mangkuk ini dengan kesehatan dan keceriaan.
                        <br/><br/>
                        Tapi jangan lewatkan daging, bahan pokok yang mengundang kenikmatan dalam setiap gigitan. Daging yang empuk dan lezat, menggoda dengan aroma menggugah selera. Kelezatannya mengalir, menghadirkan kepuasan dan kekenyangan yang tiada tara.
                        <br/><br/>
                        Kedua unsur ini, sayuran dan daging, menjadi pasangan serasi di dalam mangkuk ini. Mereka menawarkan kontras yang menarik dan melengkapi satu sama lain. Kelezatan daging memberikan kekuatan dan kepuasan, sementara sayuran memberikan kelembutan dan keseimbangan. Mangkuk ini adalah perpaduan sempurna antara kesehatan dan kenikmatan, memanjakan lidah dan memberi kebaikan bagi tubuh.
                        <br/><br/>
                        Selamat menikmati setiap sendokan dari mangkuk ini, dengan cita rasa yang mempesona dan kesenangan yang tak terlupakan.
                        </p>
                    </div>

                </div>  

                <Ralated></Ralated>
            </section>
        </Format>
    )
}